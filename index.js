function findWordOccurance(sentence){
    let wordArray = sentence.split(" ");
    let countObject = wordArray.reduce((countObj,word)=>{
        if(countObj.hasOwnProperty(word)===false){
            countObj[word] = 1;
        }else{
            countObj[word]++
        }
        return countObj;
    },{});
    
    return countObject;
}
    
console.log(findWordOccurance("the quick brown fox is quick and brown"));
    